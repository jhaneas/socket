import express from 'express'
import _ from 'lodash'
import cors from 'cors'
import bodyParser from 'body-parser'
// import moment from 'moment'

let app = express()
app.use(express.static('public'))
let port = process.env.PORT || 8890
let client = []
let connections = 0

import http from 'http'
var mysql = require('mysql')

	
var connection = mysql.createConnection({
  host:	'127.0.0.1',
  user:	'root',
  password:	'root',
  database:	'jhaneas'
})

connection.connect(function(err) {
  if (err) {
    console.error('Error:- ' + err.stack)
    return
  }

  console.log('Connected Id:- ' + connection.threadId)
})

console.log('HTTPS_ON: ', typeof process.env.HTTPS_ON)

var server = ''

if (process.env.HTTPS_ON !== 'false') {
  server = http.createServer(app)
  server.listen(port, () => {
    console.log('[INFO] Listening on *:' + port)
  })
}
else {
  server = http.createServer(app)
  server.listen(port, () => {
    console.log('[INFO] Listening on *:' + port)
  })
}



app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


let bugsnag = require('bugsnag')

const bugsnagKey = process.env.BUGSNAG || null
const appName = process.env.APPNAME || null
bugsnag.register(bugsnagKey, {
  hostname: appName
})

app.use(bugsnag.requestHandler)
app.use(bugsnag.errorHandler)

console.log('bugsnag api key: ', bugsnagKey)
var io = require('socket.io')(server)

app.get('/', function(req, res) {
  res.send('Websocket microservice')
  console.log('On welcome')
  connection.connect(function(err) {
    if (err) {
      console.error('error connecting: ' + err.stack);
      return
    }
   
    console.log('connected as id ' + connection.threadId);
  })
})

app.get('/bugsnag', function(req, res) {
  // test bugsnag details
  let msg = 'Just testing Bugsnag, you should be seeing Bugsnag message on Slack right now...'
  msg = msg + '<br />your Bugsnag api key is: ' + bugsnagKey
  msg = msg + '<br />your app name: ' + appName
  bugsnag.notify(new Error('Testing Bugsnag on ' + appName))
  res.send(msg)
})

app.get('/inventory', function (req, res) {
  connection.query('SELECT * FROM products', function (error, results, fields) {
    if (error) throw error
    res.json(results)
    // connection.end()
  })
})

app.get('/inventory/:id?', function (req, res) {
  let id = req.params.id
  let sql = 'SELECT * FROM products WHERE productCode = ' + id
  connection.query(sql, function (error, results, fields) {
    if (error) throw error
    res.json(results)
    // connection.end()
  })
})

app.post('/inventory', function (req, res) {
  console.log('body ', req.body)
  // var today = new Date()
  let d = req.body
  let data = {
    productCode: d.barcode,
    productDescription: d.description,
    productPrice: d.price,
    productQuantity: d.quantity,
    productDate: new Date()
  }
  connection.query('INSERT INTO products SET ?', data, function (error, results, fields) {
    if (error) throw error
    console.log(results.insertId)
    res.json(data)
  })
})

app.put('/inventory/:id?', function (req, res) {
  console.log('body ', req.body)
  // let id = req.params.id
  let d = req.body
  let sql = 'UPDATE products SET productDescription = ?, productPrice = ?, productLevel = ?, productQuantity = ?, productCode = ? where productCode = ?'
  let data = [d.description, d.price, d.level, d.quantity, d.barcode, d.barcode]
  connection.query(sql, data, function (error, results, fields) {
    if (error) throw error
    console.log(results.insertId)
    res.json(data)
  })
})

app.get('/twilio', function (req, res) {
  // var accountSid = 'ACe04289ef7907c2a5d04b8a96df8678e2' // Your Account SID from www.twilio.com/console
  // var authToken = '372f193b95691929773ffc907f50e8a6'   // Your Auth Token from www.twilio.com/console
  
  

  // client.messages
  //   .create({
  //     body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
  //     from: '+639215128051',
  //     to: '+639365501391'
  //   })
  //   .then(message => res.json(message.sid))
  //   .done()

  const authy = require('authy')('BiwdmH8cB4HpThdOxlVdcLYlg7dMvstj')

  authy
    .phones()
    .verification_start('09215128051', '63', 'sms', function(err, res) {
      if (err) {
        console.log(err)
      }

      console.log(res.message)
    })

})

app.post('/verify', function (req, res) {

  const authy = require('authy')('BiwdmH8cB4HpThdOxlVdcLYlg7dMvstj')
  let data = req.body
  authy
    .phones()
    .verification_start(data.mobile, '63', 'sms', function(err, d) {
      if (err) {
        console.log(err)
      }

      console.log(d.message)
      res.send(d)
    })

})

app.post('/check', function (req, res) {
  const authy = require('authy')('BiwdmH8cB4HpThdOxlVdcLYlg7dMvstj')
  let data = req.body
  authy
    .phones()
    .verification_check(data.mobile, '63', data.code, function(err, d) {
      if (err) {
        console.log(err)
      }

      console.log(d.message)
      res.send(d)
    })

})

app.post('/status', function (req, res) {
  const authy = require('authy')('BiwdmH8cB4HpThdOxlVdcLYlg7dMvstj')
  let data = req.body
  authy
    .phones()
    .verification_check(data.mobile, '63', data.code, function(err, d) {
      if (err) {
        console.log(err)
      }

      console.log(d.message)
      res.send(d)
    })

})


app.post('/register', function (req, res) {

  const authy = require('authy')('BiwdmH8cB4HpThdOxlVdcLYlg7dMvstj')
  let data = req.body
  authy
    .register_user(data.email, data.mobile, '63', function (err, d) {
      // res = {user: {id: 1337}} where 1337 = ID given to use, store this someplace
      console.log(d.message)
      res.send(d)
    })

})




io.sockets.on('connection', function (socket) {

  var CronJob = require('cron').CronJob
  new CronJob('0 */1 * * * *', function() {
    setData()
  }, null, true, 'America/Los_Angeles')


  socket.on('loginconnection', function (data) {
    console.log('incoming connection initiated by', data.id)
    // check if already connected
    if (_.filter(client, ['id', data.id]).length === 0) {
      socket.broadcast.emit('notice', true)
    }
    else if (_.filter(client, ['id', data.id]).length === 1) {
      _.remove(client, _.filter(client, ['id', data.id])[0])
      socket.broadcast.emit('notice', false)
    }
  })

  console.log('Socket Connection Started. Connections: ' + connections)
  socket.on('login', function (data) {
    let handshake = socket.handshake
    console.log('incoming connection initiated by', data.id)
    // check if already connected
    if ((_.filter(client, ['id', data.id]).length === 0) && (data.job === 'login')) {
      let dataToAppend = {
        id: data.id,
        job: data.job,
        ip: handshake.headers.origin,
        userAgent: handshake.headers['user-agent']
      }
      client.push(dataToAppend)
      connections++
      console.log('ids', client)
    }
    else if ((_.filter(client, ['id', data.id]).length === 1) && (data.job === 'logout')) {
      _.remove(client, _.filter(client, ['id', data.id])[0])
      connections--
      console.log('id', client)
    }
    io.sockets.emit('clients', client)
  })

  socket.on('setData', function (data) {
    console.log('data ', data)
    io.sockets.emit('getData', data)
  })

  function setData () {
    console.log('results ')
    connection.query('SELECT * FROM products', function (error, results, fields) {
      if (error) throw error
      console.log('results ', results)
      io.sockets.emit('getProduct', results)
      // connection.end()
    })
  }

  socket.on('getLogs', function (data) {
    console.log('data ', data)
    io.sockets.emit('getData', data)
  })


  // check to see if online
  socket.on('hello', function () {
    io.sockets.emit('hello-back', {})
  })

})
