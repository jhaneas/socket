#!/bin/bash

# Maintainer John Maclaine Danao @ 1722 IT Services <johnmaclained@gmail.com>

tput setaf 3; echo "********************************* "
tput setaf 3; echo "*****    1722 ITSERVICES    ***** "
tput setaf 3; echo "***** SSL Generation Script ***** "
tput setaf 3; echo "*****        OPENSSL        ***** "
tput setaf 3; echo "********************************* "
echo
echo
echo -n "Enter the FQDN to be used: [myapp.local] "
read fqdn
tput setaf 6; echo "NOTE: "$fqdn "will be used later for Common Name [CN] so remember your FQDN"
tput setaf 3; echo -n "Enter the filename to set for all certificates: [myapp] "
read filename
echo
echo


# Generating private key 
key="key"
privatekey=$filename$key
openssl genrsa -out $privatekey.pem 2048





# Generating CA using private key for encrypting
ca="ca"
openssl req -new -x509 -days 3650 -nodes -key $privatekey.pem -sha256 -out $filename$ca.pem





# Generating Certificate Signing Request [csr] Configuration
echo
tput setaf 6; echo "Enter the following configuration again. "
tput setaf 6; echo "It must Match from previous"
echo
echo "Enter Country Name: "
read c
echo "Enter State or Province: "
read st
echo "Enter Locality: "
read l
echo "Enter Organization Name: "
read o
echo "Enter Organizational Unit: "
read ou
echo "Enter EmailAddress: "
read ea
printf "[req]\ndefault_bits=2048\nprompt=no\ndefault_md=sha256\ndistinguished_name=dn\n\n[dn]\nC=$c\nST=$st\nL=$l\nO=$o\nOU=$ou \nemailAddress=$ea\nCN=$fqdn" > $filename.csr.cnf





# Generating Extended file .ext for server Subject Alt Name
printf "authorityKeyIdentifier=keyid,issuer\nbasicConstraints=CA:FALSE\nkeyUsage=digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment\nsubjectAltName=@alt_names\n\n[alt_names]\nDNS.1=$fqdn" > $filename.ext






# Generating  sl_certificate_key .key & Certificate Signing Request [csr] 
echo "Generating ssl_certificate_key .key &Certificate Signing Request [csr] "
openssl req -new -sha256 -nodes -out $filename.csr -newkey rsa:2048 -keyout $filename.key -config <(cat $filename.csr.cnf )





# Generating ssl_certificate .crt
# and using the privatekey,CA,csr,ext in applying encryption
echo "Generating ssl_certificate .crt && serial"
echo "and using the privatekey,CA,csr,ext in applying encryption"
openssl x509 -req -in $filename.csr -CA $filename$ca.pem -CAkey $privatekey.pem -CAcreateserial -out $filename.crt -days 500 -sha256 -extfile $filename.ext

tput setaf 6; echo "******************************************* "
tput setaf 6; echo "*****              DONE !              "
tput setaf 6; echo "*****        SSL Generation Script     "
tput setaf 6; echo "*****              OPENSSL             "
tput setaf 6; echo "*****         You can now use the ff:  "
tput setaf 6; echo "*****     $filename.key for key             "
tput setaf 6; echo "*****     $filename.crt for cert            "
tput setaf 6; echo "*****     $filename$ca.pem on your browsers "
tput setaf 6; echo "*****                                       "
tput setaf 6; echo "************************************************ "




